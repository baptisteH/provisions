import React from 'react';
import { View } from 'react-native';
import { Router, Stack, Scene, Tabs } from 'react-native-router-flux';

import { Container } from 'native-base';

import { AppLoading } from 'expo';
import * as Font from 'expo-font';
import { Ionicons } from '@expo/vector-icons';

import Footer from './src/components/Footer';

import HomeStock from './src/pages/stock/HomeStock';
import ProductDetails from './src/pages/stock/ProductDetails';
import HomeLists from './src/pages/lists/HomeLists';
import HomeUser from './src/pages/user/HomeUser';

console.disableYellowBox = true;

const styles = {
  headerStyle: {
    
    height: 55,
    paddingVertical: 5,
    
    backgroundColor: '#fff',
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    borderBottomColor: 'transparent',
    
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 1,
    shadowRadius: 0,
    shadowColor: '#b5ecee',
  },
}

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isReady: false,
    };
  }
  
  async componentDidMount() {
    await Font.loadAsync({
      Roboto: require('native-base/Fonts/Roboto.ttf'),
      Roboto_medium: require('native-base/Fonts/Roboto_medium.ttf'),
      Avenir_Light: require('./src/assets/fonts/Avenir-Light.otf'),
      Avenir_Medium: require('./src/assets/fonts/Avenir-Medium.otf'),
      Avenir_Black: require('./src/assets/fonts/Avenir-Black.otf'),
      ...Ionicons.font,
    });
    this.setState({ isReady: true });
  }
  
  render() {
    if (!this.state.isReady) {
      return <AppLoading />;
    }
    
    return (
        <View style={{flex: 1}}>
          <Router>
            <Stack key="root"
                   headerStyle={styles.headerStyle}>
              <Scene
                  key="homestock"
                  component={HomeStock}
                  title="Stock"
                  initial />
              <Scene
                  key="details"
                  component={ProductDetails}
                  title="" />
              <Scene
                  key="homelists"
                  component={HomeLists}
                  title="Listes" />
              <Scene
                  key="homeuser"
                  component={HomeUser}
                  title="Compte" />
            </Stack>
          </Router>
          <Footer />
        </View>
    );
  }
}
