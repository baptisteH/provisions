import PropTypes from 'prop-types';
import React, { useState } from 'react';
import { ActivityIndicator, Dimensions, Image, ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native';

import ProductsCategories from '../../data/categories';
import ProductsStorages from '../../data/storages';

const { width } = Dimensions.get('window');

const styles = StyleSheet.create({
    scrollView: {
        backgroundColor: '#F8FDFE',
    },
    container: {
        flex: 1,
        backgroundColor: 'transparent',
    },
    imageContainer: {
        width: width,
        paddingVertical: 10,
        backgroundColor: '#fff'
    },
    image: {
        height: 'auto',
        width: '100%',
        aspectRatio: 16/9,
        resizeMode: 'contain',
    },
    nameContainer: {
        width,
        paddingTop: 30,
        paddingHorizontal: 10,
    },
    nameText: {
        fontSize: 24,
        fontWeight: 'bold',
        textAlign: 'center',
    },
    quantityContainer: {
        paddingVertical: 30,
    },
    quantityNumberContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    quantityText: {
        paddingHorizontal: 30,
        fontSize: 36,
    },
    quantityButton: {
        width: 30,
        height: 30,
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: 'black',
        borderWidth: 1,
        borderRadius: 100,
    },
    quantityButtonText: {
        fontSize: 22,
        lineHeight: 24,
        paddingLeft: 2,
    },
    informationsContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 20,
        paddingVertical: 10,
    },
    informationContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        paddingVertical: 20,
        borderColor: '#ddd',
        borderWidth: 1,
    },
    informationType: {
        marginBottom: 15,
        textTransform: 'uppercase',
        color: '#3DBABE',
        fontSize: 15,
    },
    informationText: {
        fontSize: 18,
    }
});

const ProductDetails = ({ product }) => {
    
    if (!product) {
        return (
            <View style={styles.container}>
                <ActivityIndicator size="large" color="#3DBABE" />
            </View>
        );
    }
    
    const [quantity, setQuantity] = useState(product.quantity);
    
    const category = ProductsCategories[product.category].name;
    const storage = ProductsStorages[product.storage].name;
    
    return (
        <ScrollView style={styles.scrollView}>
            <View style={styles.container}>
                <View style={styles.imageContainer}>
                    <Image source={{ uri: product.image }} style={styles.image} />
                </View>
                <View style={styles.nameContainer}>
                    <Text style={styles.nameText}>{product.name}</Text>
                </View>
                <View style={styles.quantityContainer}>
                    <View style={styles.quantityNumberContainer}>
                        <TouchableOpacity onPress={() => setQuantity(quantity-1)}>
                            <View style={styles.quantityButton}>
                                <Text style={styles.quantityButtonText}>-</Text>
                            </View>
                        </TouchableOpacity>
                        <Text style={styles.quantityText}>{quantity}</Text>
                        <TouchableOpacity onPress={() => setQuantity(quantity+1)}>
                            <View style={styles.quantityButton}>
                                <Text style={styles.quantityButtonText}>+</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={styles.informationsContainer}>
                    <View style={styles.informationContainer}>
                        <Text style={styles.informationType}>Catégorie</Text>
                        <Text style={styles.informationText}>{category}</Text>
                    </View>
                    <View style={styles.informationContainer}>
                        <Text style={styles.informationType}>Stockage</Text>
                        <Text style={styles.informationText}>{storage}</Text>
                    </View>
                </View>
            </View>
        </ScrollView>
    );
}

ProductDetails.propTypes = {
    product: PropTypes.object.isRequired,
};

export default ProductDetails;