import React, { useState } from 'react';
import { Dimensions, StyleSheet, View } from 'react-native'
import { Fab, Icon, Tab, Tabs } from 'native-base'

import StockItems from '../../components/StockItems';
import ProductModal from '../../components/ProductModal';

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F8FDFE',
    },
    tabs: {
        paddingTop: 20,
        backgroundColor: "transparent",
    },
    tab: {
        backgroundColor: '#F8FDFE',
    },
    activeTab: {
        backgroundColor: '#F8FDFE',
    },
    tabBarUnderline: {
        borderBottomWidth: 4,
        borderBottomColor: '#68CE84',
    },
    activeText: {
        color: '#68CE84',
    },
    fab: {
        backgroundColor: '#55B46F'
    },
    fabIcon: {
        fontSize: 30, paddingTop: 8
    }
});

const HomePage = () => {
    
    const modal = React.useRef();
    const [searchText, setSearchText] = useState('');
    
    return (
        <View style={styles.container}>
            <Tabs initialPage={1}
                  locked
                  style={styles.tabs}
                  tabBarUnderlineStyle={styles.tabBarUnderline}
                  onChangeTab={() => setSearchText('')}>
                <Tab heading="Congelateur"
                     tabStyle={styles.tab}
                     activeTabStyle={styles.activeTab}
                     activeTextStyle={styles.activeText}>
                    <StockItems storage={0}
                                searchText={searchText}/>
                </Tab>
                <Tab heading="Frigo"
                     tabStyle={styles.tab}
                     activeTabStyle={styles.activeTab}
                     activeTextStyle={styles.activeText}>
                    <StockItems storage={1}
                                searchText={searchText}/>
                </Tab>
                <Tab heading="Placards"
                     tabStyle={styles.tab}
                     activeTabStyle={styles.activeTab}
                     activeTextStyle={styles.activeText}>
                    <StockItems storage={2}
                                searchText={searchText}/>
                </Tab>
            </Tabs>
            <ProductModal ref={modal} />
            <Fab style={styles.fab}
                 position="bottomRight"
                 onPress={() => (modal.current.show())}>
                <Icon name="add" style={styles.fabIcon}/>
            </Fab>
        </View>
    );
    
}

export default HomePage;