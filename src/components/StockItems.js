import PropTypes from 'prop-types';
import React, { Component  } from 'react'
import { Actions } from 'react-native-router-flux';
import { Dimensions, ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { Icon, Input, Item } from 'native-base'

import ProductCard from './ProductCard'

import ProductsList from '../data/products'

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
    scrollView: {
        backgroundColor: '#F8FDFE',
    },
    pageContainer: {
        flex: 1,
        backgroundColor: 'transparent',
    },
    pageTitle: {
        paddingTop: 25,
        paddingBottom: 5,
        paddingLeft: 15,
        fontSize: 24,
    },
    productsContainer: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        paddingVertical: 10,
        paddingHorizontal: 10,
    },
    divider: {
        height: 1,
        width: width - 30,
        backgroundColor: '#b5ecee',
        marginTop: 15,
        marginBottom: 10,
        marginHorizontal: 15,
    },
    searchBar: {
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: 10,
    },
    searchInput: {
        paddingLeft: 10,
    }
});

export default class StockItems extends Component {
    
    goToDetails = (product) => {
        Actions.details({
            product,
        });
    };
    
    state = {
        searchText: '',
    };
    
    render() {
        
        const currentStorageList = ProductsList.filter((item) => item['storage'] === this.props.storage);
        const alertProducts = currentStorageList.filter((item) => item['alert'] ? item['alert'] : false);
        const normalProducts = currentStorageList.filter((item) => !item['alert']);
        
        // console.log(this.state.searchText);
        
        let filteredProducts = [];
        
        if (this.state.searchText !== '') {
            
            filteredProducts = currentStorageList.filter((item) => {
                console.log(item['name'].indexOf(this.state.searchText) > -1);
                return item['name'].indexOf(this.state.searchText) > -1;
            });
            
        }
        
        return (
            <ScrollView style={ styles.scrollView }>
                <View style={ styles.pageContainer }>
                    
                    <View>
                        <Item style={styles.searchBar}>
                            <Icon type="FontAwesome" name='search' />
                            <Input placeholder="Rechercher..."
                                   style={styles.searchInput}
                                   onChangeText={(text) => this.setState({searchText: text})}
                                   value={this.state.searchText} />
                        </Item>
                    </View>
                    
                    {this.state.searchText === '' ? (
                        <View>
                            {alertProducts.length > 0 && (
                                <View style={ styles.pageContainer }>
                                    <Text style={ styles.pageTitle }>Alertes</Text>
                                    <View style={ styles.productsContainer }>
                                        { alertProducts
                                        .map((product, index) => (
                                            <TouchableOpacity key={index} onPress={() => this.goToDetails(product)}>
                                                <ProductCard
                                                    name={ product.name }
                                                    image={ product.image }
                                                    quantity={ product.quantity }
                                                    alert
                                                />
                                            </TouchableOpacity>
                                        )) }
                                    </View>
                                    <View style={ styles.divider } />
                                </View>
                            )}
                            <View style={ styles.productsContainer }>
                                { normalProducts
                                .map((product, index) => (
                                    <TouchableOpacity key={index} onPress={() => this.goToDetails(product)}>
                                        <ProductCard
                                            name={ product.name }
                                            image={ product.image }
                                            quantity={ product.quantity }
                                        />
                                    </TouchableOpacity>
                                )) }
                            </View>
                        </View>
                    ):(
                        <View style={ styles.productsContainer }>
                            { filteredProducts
                            .map((product, index) => (
                                <TouchableOpacity key={index} onPress={() => this.goToDetails(product)}>
                                    <ProductCard
                                        name={ product.name }
                                        image={ product.image }
                                        quantity={ product.quantity }
                                    />
                                </TouchableOpacity>
                            )) }
                        </View>
                    )}
                </View>
            </ScrollView>
        );
    }
}

StockItems.propTypes = {
    storage: PropTypes.number
};