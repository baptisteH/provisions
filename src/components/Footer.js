import React, { Component } from 'react';
import { StyleSheet, View } from 'react-native'
import { Actions } from 'react-native-router-flux';
import { Footer, FooterTab, Button, Icon, Text } from 'native-base';

const styles = StyleSheet.create({
    footer: {
        height: 60,
        marginTop: 0,
        backgroundColor: '#fff',
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        borderTopColor: 'transparent',
        shadowOffset: {
            width: 0,
            height: -3,
        },
        shadowOpacity: 1,
        shadowRadius: 0,
        shadowColor: '#b5ecee',
    },
    footerTab: {
        marginTop: -10,
    }
});

export default class CustomFooter extends Component {
    
    goto = (key) => {
        Actions.reset(key)
        this.setState({currentScene: key})
    }
    
    render() {
        
        return (
            <View>
                <Footer style={styles.footer}>
                    <FooterTab style={styles.footerTab}>
                        <Button vertical onPress={ () => this.goto('homeuser') }>
                            <Icon name="person" />
                            <Text>Profil</Text>
                        </Button>
                        <Button vertical onPress={ () => this.goto('homestock') }>
                            <Icon active name="home" />
                            <Text>Stock</Text>
                        </Button>
                        <Button vertical onPress={ () => this.goto('homelists') }>
                            <Icon name="document" />
                            <Text>Listes</Text>
                        </Button>
                    </FooterTab>
                </Footer>
            </View>
        );
    }
}