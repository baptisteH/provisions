import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { Dimensions, Image, StyleSheet } from 'react-native'
import { Body, Card, CardItem, Thumbnail, Text, View } from 'native-base';

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
    card: {
        flexGrow: 0,
        flexShrink: 0,
        flexBasis: '33%',
        width: width/3 - 17,
        marginLeft: 5,
        marginRight: 5,
        borderRadius: 5,
        borderWidth: 1,
        borderColor: '#b5ecee',
        shadowOffset: {
            width: -3,
            height: 3,
        },
        shadowOpacity: 1,
        shadowRadius: 0,
        shadowColor: '#b5ecee'
    },
    cardAlert: {
        borderColor: '#F6BC43',
        shadowColor: '#F6BC43'
    },
    cardNoQuantity: {
        opacity: .5,
    },
    cardItem: {
        position: 'relative',
        flexGrow: 0,
        flexShrink: 0,
        flexBasis: '33%',
        paddingTop: 8,
        paddingBottom: 8,
        paddingLeft: 8,
        paddingRight: 8,
        overflow: 'hidden',
        borderRadius: 5,
        backgroundColor: 'white',
        
    },
    cardImg: {
        height: width/3 - 31,
        width: '100%',
        opacity: .9,
        borderRadius: 5,
        overflow: 'hidden',
        resizeMode: 'contain',
    },
    cardName: {
        width: '100%',
        paddingTop: 10,
        paddingBottom: 10,
        textAlign: 'center',
        color: '#3B301B',
    },
    cardQuantity: {
        position: 'absolute',
        bottom: 0,
        right: 0,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        height: 32,
        width: 37,
    },
    cardQuantityText: {
        color: '#3B301B',
        fontSize: 18,
        fontWeight: 'bold',
        textShadowColor: '#fff',
        textShadowRadius: 0,
        textShadowOffset: {
            width: 2,
            height: 2,
        },
    },
    cardQuantitySpan: {
        fontWeight: '400',
        opacity: .5,
        letterSpacing: 1,
    }
});

const ProductCard = ({image, quantity, alert}) => {
    
    return (
        <Card style={[
            styles.card,
            alert && styles.cardAlert,
            quantity === 0 && styles.cardNoQuantity ]}>
            <CardItem style={styles.cardItem}>
                <Thumbnail style={styles.cardImg}
                           source={{uri: image}} />
                <View style={styles.cardQuantity}>
                    <Text style={styles.cardQuantityText}>
                        <Text style={styles.cardQuantitySpan}>
                            x
                        </Text>
                        {quantity}
                    </Text>
                </View>
            </CardItem>
        </Card>
    );
}

ProductCard.propTypes = {
    alert: PropTypes.bool,
    image: PropTypes.string,
    quantity: PropTypes.quantity,
};

ProductCard.defaultProps = {
    alert: false,
    image: 'https://image.shutterstock.com/image-vector/abstract-blurred-gradient-mesh-background-260nw-460041640.jpg',
    quantity: 0,
};

export default ProductCard;