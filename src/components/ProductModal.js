import PropTypes from 'prop-types';
import React from 'react';
import { Alert, Dimensions, Modal, StyleSheet, Text, TouchableHighlight, View } from 'react-native';
import { Icon, Input, Item, Label, Picker } from 'native-base';

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 22,
    },
    modalView: {
        margin: 20,
        backgroundColor: "white",
        borderRadius: 20,
        padding: 35,
        alignItems: "center",
        justifyContent: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        width,
        height,
    },
    openButton: {
        paddingVertical: 12,
        paddingHorizontal: 40,
        marginHorizontal: 10,
        borderRadius: 30,
        elevation: 2,
    },
    buttonText: {
        color: "white",
        fontWeight: "bold",
        textAlign: "center",
        fontSize: 18,
    },
    modalTitle: {
        marginBottom: 60,
        fontSize: 24,
        fontWeight: '600',
        textAlign: "center",
    },
    buttonsContainer: {
        flexDirection: 'row',
        marginTop: 20,
    },
    inputsContainer: {
        width: '100%',
        paddingHorizontal: 20,
        marginBottom: 60,
    },
    itemInput: {
        marginBottom: 35,
    },
    input: {
        paddingLeft: 10,
    },
    itemInputLabel: {
        paddingLeft: 10,
        marginBottom: 10,
    },
    picker: {
        width: width - 100,
        paddingLeft: 0,
        marginTop: 10,
        marginBottom: 0,
    }
});

class ProductModal extends React.Component {
    state = {
        modalVisible: false,
        inputName: '',
        inputImage: '',
        inputCategory: '',
        inputStorage: undefined,
    };
    
    onStorageChange(value) {
        this.setState({
            inputStorage: value
        });
    }
    
    show = () => {
        this.setState({
            modalVisible: true,
        });
    };
    
    hide = () => {
        this.setState({
            modalVisible: false,
        });
    };
    
    render() {
        const { modalVisible } = this.state;
        
        return (
            <Modal
                animationType="slide"
                transparent={true}
                visible={modalVisible}
                onRequestClose={() => {
                    Alert.alert("Modal has been closed.");
                }}>
                <View style={styles.centeredView}>
                    <View style={styles.modalView}>
                        
                        <Text style={styles.modalTitle}>Ajouter un produit</Text>
                        
                        <View style={styles.inputsContainer}>
                            <Item floatingLabel style={styles.itemInput}>
                                <Label style={styles.itemInputLabel}>Nom</Label>
                                <Input style={styles.input}
                                       onChangeText={(text) => this.setState({inputName: text})}
                                       value={this.state.inputName} />
                            </Item>
                            <Item floatingLabel style={styles.itemInput}>
                                <Label style={styles.itemInputLabel}>Image</Label>
                                <Input style={styles.input}
                                       onChangeText={(text) => this.setState({inputImage: text})}
                                       value={this.state.inputImage} />
                            </Item>
                            <Item floatingLabel style={styles.itemInput}>
                                <Label style={styles.itemInputLabel}>Catégorie</Label>
                                <Input style={styles.input}
                                       onChangeText={(text) => this.setState({inputCategory: text})}
                                       value={this.state.inputCategory} />
                            </Item>
                            <Item picker>
                                <Picker
                                    placeholder="Stockage"
                                    mode="dropdown"
                                    iosIcon={<Icon name="arrow-down" />}
                                    style={styles.picker}
                                    placeholderStyle={{ color: "#000" }}
                                    placeholderIconColor="#000"
                                    selectedValue={this.state.inputStorage}
                                    onValueChange={this.onStorageChange.bind(this)}
                                >
                                    <Picker.Item label="Congélateur" value="0" />
                                    <Picker.Item label="Frigo" value="1" />
                                    <Picker.Item label="Placard" value="2" />
                                </Picker>
                            </Item>
                        </View>
                        
                        <View style={styles.buttonsContainer}>
                            <TouchableHighlight
                                style={{ ...styles.openButton, backgroundColor: "#ccc" }}
                                onPress={() => {
                                    this.hide();
                                }}>
                                <Text style={styles.buttonText}>Annuler</Text>
                            </TouchableHighlight>
                            <TouchableHighlight
                                style={{ ...styles.openButton, backgroundColor: "#55B46F" }}
                                onPress={() => {
                                    this.hide();
                                }}>
                                <Text style={styles.buttonText}>Ajouter</Text>
                            </TouchableHighlight>
                        </View>
                    
                    </View>
                </View>
            </Modal>
        );
    };
}

ProductModal.propTypes = {

};

export default ProductModal;
